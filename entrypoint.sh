#!/bin/bash

sed -e 's/^fti=couchdb-external-hook.py$/fti=\/usr\/bin\/python \/usr\/local\/etc\/couchdb\/couchdb-external-hook.py --remote-host 172.17.0.1 --remote-port 5985 /' -i /usr/local/etc/couchdb/local.ini

sh /tmp/create-couchdb-admin-user.sh & couchdb > /dev/null
